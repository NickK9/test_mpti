from sqlalchemy.orm import Session

import models, schemas

def get_robot(db: Session, robot_id: int):
    return db.query(models.Robots).filter(models.Robots.id == robot_id).first()

def get_robots(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Robots).offset(skip).limit(limit).all()

def create_robot(db: Session, robot: schemas.RobotCreate):
    db_item = models.Robots(name=robot.name, 
        price=robot.price, is_sold=robot.is_sold, 
        description=robot.description)
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item

def get_robot_by_name(db: Session, name: str):
    return db.query(models.Robots).filter(models.Robots.name == name).first()


def update_robots(db: Session, robot_id: int, data: dict):
    query = db.query(models.Robots).filter(models.Robots.id == robot_id).update(data)
    db.commit()
    return query
