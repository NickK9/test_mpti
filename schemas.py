from typing import List, Optional

from pydantic import BaseModel


class RobotBase(BaseModel):
    name: str
    price: int
    is_sold: bool
    description: str


class RobotCreate(RobotBase):
    pass


class Robot(RobotBase):
    id: int

    class Config:
        orm_mode = True