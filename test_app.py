from fastapi import FastAPI, HTTPException, status, Body, Depends
from typing import Optional, List
from sqlalchemy.orm import Session
from pydantic import BaseModel
from fastapi.encoders import jsonable_encoder
import crud, models, schemas
from database import SessionLocal, engine
from sqlalchemy.orm import Session

import xmltodict

models.Base.metadata.create_all(bind=engine)

class Robot(BaseModel):
    name: str 
    price: int 
    is_sold: bool 
    description: str

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/create_robot/", response_model=schemas.Robot)
def create_robot(robot: schemas.RobotCreate, db: Session = Depends(get_db)):
    '''Создание робота'''
    db_user = crud.get_robot_by_name(db, name=robot.name)
    if db_user:
        raise HTTPException(status_code=400, detail="This name already registered")
    return crud.create_robot(db=db, robot=robot)

@app.get("/get_robot/{id}/{data_type}/")
async def get_robot_info(id: int, data_type: str, db: Session = Depends(get_db)):
    db_user = crud.get_robot(db, robot_id=id)
    '''Взять робота бо id и выбрать формат данных для получения результата'''
    if db_user is None:
        raise HTTPException(status_code=404, detail="Robot not found")
    elif data_type == "json":
        return {'id': db_user.id,
            'name': db_user.name,
            'price': db_user.price,
            'is_sold': db_user.is_sold,
            'description': db_user.description}
    elif data_type == "xml":
        my_dict = {'response': {'id': db_user.id,
            'name': db_user.name,
            'price': db_user.price,
            'is_sold': db_user.is_sold,
            'description': db_user.description}}
        return xmltodict.unparse(my_dict) 
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid id or data type"
        )

@app.get("/robots/", response_model=List[schemas.Robot])
def read_robots(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    '''Все данные + limit + offset'''
    robots = crud.get_robots(db, skip=skip, limit=limit)
    return robots

@app.put("/update_robot/{robot_id}/")
async def update_robot(robot_id: int, robot: Robot, db: Session = Depends(get_db)):
    '''Обновить данные у робота по id'''
    update_robot_encoded = jsonable_encoder(robot)
    robots = crud.update_robots(db, robot_id, update_robot_encoded)
    return robots
